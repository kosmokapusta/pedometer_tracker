package po.csc.ciklum.com.pedometer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import au.com.bytecode.opencsv.CSVWriter;


public class TrackSensorsService extends Service {

    private SensorManager mSensorManager;

    private Sensor mAccelerSensor;
    private Sensor mGravitySensor;
    private Sensor mGyroSensor;
    private Sensor mLinearAccelerSensor;
    private Sensor mMagneticSensor;
    private Sensor mStepCounterSensor;
    private Sensor mUncalibratedGyroSensor;
    private Sensor mRotationSensor;
    private Sensor mUncalibratedMagneticSensor;

    private CSVWriter mWriter;
    File fileDir = (Environment.getExternalStorageDirectory());
    private File mFile = new File(fileDir, "test.csv");

    String sensorType;
    String axisX;
    String axisY;
    String axisZ;

    String radioGroupText1;
    String radioGroupText2;
    String radioGroupText3;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, getResources().getString(R.string.service_started), Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ArrayList<String> optionsList = intent.getStringArrayListExtra("Options");
        radioGroupText1 = optionsList.get(0);
        radioGroupText2 = optionsList.get(1);
        radioGroupText3 = optionsList.get(2);

        setUpSensors();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mListener);
        }

        Toast.makeText(this, getResources().getString(R.string.service_stopped), Toast.LENGTH_LONG).show();
    }

    private final SensorEventListener mListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            switch (event.sensor.getType()){
                case Sensor.TYPE_ACCELEROMETER:
                    sensorType = "Acceleration";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                    sensorType = "Magnetometer";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_GRAVITY:
                    sensorType = "Gravity";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    sensorType = "Rotation_Rate";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                    sensorType = "Gyroscope";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    sensorType = "User_Acceleration";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    sensorType = "Calibrated_Magnetic_Field";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
                case Sensor.TYPE_STEP_COUNTER:
                    sensorType = "Steps";
                    axisX = Float.toString(event.values[0]);
                    axisY = "";
                    axisZ = "";
                    break;
                case Sensor.TYPE_ROTATION_VECTOR:
                    sensorType = "Attitude";
                    axisX = Float.toString(event.values[0]);
                    axisY = Float.toString(event.values[1]);
                    axisZ = Float.toString(event.values[2]);
                    break;
            }

            mWriter.writeNext(new String[]{Long.toString(System.currentTimeMillis()/1000L), sensorType, radioGroupText1, radioGroupText2, radioGroupText3, axisX, axisY, axisZ}, false);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    public void setUpSensors() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mAccelerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGravitySensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mLinearAccelerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mStepCounterSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        mUncalibratedGyroSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mUncalibratedMagneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        mSensorManager.registerListener(mListener, mAccelerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mGravitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mGyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mLinearAccelerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mMagneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mStepCounterSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mUncalibratedGyroSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mRotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(mListener, mUncalibratedMagneticSensor, SensorManager.SENSOR_DELAY_NORMAL);

        try {
            if(mFile.exists()) {
                mFile.delete();
            }
            mFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (mWriter == null) {
                mWriter = new CSVWriter(new FileWriter(mFile, true));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}