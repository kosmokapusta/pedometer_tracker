package po.csc.ciklum.com.pedometer;

import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;


public class MyActivity extends ActionBarActivity {
    RadioGroup radioGroup1;
    RadioGroup radioGroup2;
    RadioGroup radioGroup3;
    RadioGroup radioGroup4;
    RadioGroup radioGroup5;

    RadioGroup activeRadioGroup;

    String radioGroupText1;
    String radioGroupText2;
    String radioGroupText3;

    Button buttonStart;

    File fileDir = (Environment.getExternalStorageDirectory());
    private File mFile = new File(fileDir, "test.csv");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        radioGroup1 = (RadioGroup)findViewById(R.id.radioGroup1);
        radioGroup2 = (RadioGroup)findViewById(R.id.radioGroup2);
        radioGroup3 = (RadioGroup)findViewById(R.id.radioGroup3);
        radioGroup4 = (RadioGroup)findViewById(R.id.radioGroup4);
        radioGroup5 = (RadioGroup)findViewById(R.id.radioGroup5);

        radioGroup1.setOnCheckedChangeListener(listener1);
        radioGroup2.setOnCheckedChangeListener(listener1);
        radioGroup3.setOnCheckedChangeListener(listener1);
        radioGroup4.setOnCheckedChangeListener(listener2);
        radioGroup5.setOnCheckedChangeListener(listener3);

        buttonStart = (Button) findViewById(R.id.buttonStart);


    }

    private RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activeRadioGroup = group;
                radioGroup4.setOnCheckedChangeListener(null);
                radioGroup5.setOnCheckedChangeListener(null);
                radioGroup4.clearCheck();
                radioGroup5.clearCheck();
                radioGroup4.setOnCheckedChangeListener(listener2);
                radioGroup5.setOnCheckedChangeListener(listener3);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activeRadioGroup = group;
                radioGroup1.setOnCheckedChangeListener(null);
                radioGroup2.setOnCheckedChangeListener(null);
                radioGroup3.setOnCheckedChangeListener(null);
                radioGroup5.setOnCheckedChangeListener(null);
                radioGroup1.clearCheck();
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup5.clearCheck();
                radioGroup1.setOnCheckedChangeListener(listener1);
                radioGroup2.setOnCheckedChangeListener(listener1);
                radioGroup3.setOnCheckedChangeListener(listener1);
                radioGroup5.setOnCheckedChangeListener(listener3);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener listener3 = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                activeRadioGroup = group;
                radioGroup1.setOnCheckedChangeListener(null);
                radioGroup2.setOnCheckedChangeListener(null);
                radioGroup3.setOnCheckedChangeListener(null);
                radioGroup4.setOnCheckedChangeListener(null);
                radioGroup1.clearCheck();
                radioGroup2.clearCheck();
                radioGroup3.clearCheck();
                radioGroup4.clearCheck();
                radioGroup1.setOnCheckedChangeListener(listener1);
                radioGroup2.setOnCheckedChangeListener(listener1);
                radioGroup3.setOnCheckedChangeListener(listener1);
                radioGroup4.setOnCheckedChangeListener(listener2);
            }
        }
    };

    public void doStartClick(View view) throws IOException {
        ArrayList<String> optionsArray = new ArrayList<String>();

        if (isMyServiceRunning()) {
            Intent intent = new Intent(this, TrackSensorsService.class);
            stopService(intent);
            removeLastLine();
            buttonStart.setText(getResources().getString(R.string.start));
        } else {
            if (activeRadioGroup == radioGroup1 || activeRadioGroup == radioGroup2 || activeRadioGroup == radioGroup3) {
                if (radioGroup1.getCheckedRadioButtonId() > 0 && radioGroup2.getCheckedRadioButtonId() > 0 && radioGroup3.getCheckedRadioButtonId() > 0) {
                    radioGroupText1 = getButtonTextById(radioGroup1.getCheckedRadioButtonId());
                    radioGroupText2 = getButtonTextById(radioGroup2.getCheckedRadioButtonId());
                    radioGroupText3 = getButtonTextById(radioGroup3.getCheckedRadioButtonId());
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.three_options_toast), Toast.LENGTH_SHORT).show();
                }
            } else if (activeRadioGroup == radioGroup4) {
                radioGroupText1 = getButtonTextById(radioGroup4.getCheckedRadioButtonId());
                radioGroupText2 = "";
                radioGroupText3 = "";
            } else if (activeRadioGroup == radioGroup5) {
                radioGroupText1 = getButtonTextById(radioGroup5.getCheckedRadioButtonId());
                radioGroupText2 = "";
                radioGroupText3 = "";
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.select_options_toast), Toast.LENGTH_SHORT).show();
            }

            if (radioGroupText1 != null || radioGroupText2 != null || radioGroupText3 != null) {
                optionsArray.add(radioGroupText1);
                optionsArray.add(radioGroupText2);
                optionsArray.add(radioGroupText3);

                Intent intent = new Intent(this, TrackSensorsService.class);
                intent.putExtra("Options", optionsArray);
                startService(intent);
            }

            buttonStart.setText(getResources().getString(R.string.stop));
        }

    }

    public void doDeleteClick(View view) {
        if(mFile.exists()) {
            mFile.delete();
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.file_deleted), Toast.LENGTH_SHORT).show();
        }
    }

    public String getButtonTextById(int id) {
        RadioButton radioButton = (RadioButton)findViewById(id);

        return (String) radioButton.getText();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, TrackSensorsService.class);
        stopService(intent);
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("po.csc.ciklum.com.pedometer.TrackSensorsService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void removeLastLine() throws IOException {
        RandomAccessFile f = new RandomAccessFile(mFile, "rw");
        byte b;
        long length = f.length() - 1;
        do {
            length -= 1;
            f.seek(length);
            b = f.readByte();
        } while(b != 10 && length > 0);
        try {
            f.setLength(length+1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}